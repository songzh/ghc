#include "Stg.h"

StgWord8 hs_mulIntMayOflo8(StgWord8 _0, StgWord8 _1) { return mulIntMayOflo(_0, _1); }

StgWord16 hs_mulIntMayOflo16(StgWord16 _0, StgWord16 _1) { return mulIntMayOflo(_0, _1); }

StgWord32 hs_mulIntMayOflo32(StgWord32 _0, StgWord32 _1) { return mulIntMayOflo(_0, _1); }

StgWord64 hs_mulIntMayOflo64(StgWord64 _0, StgWord64 _1) { return mulIntMayOflo(_0, _1); }
